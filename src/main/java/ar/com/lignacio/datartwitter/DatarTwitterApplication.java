package ar.com.lignacio.datartwitter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication
public class DatarTwitterApplication {

	public static void main(String[] args) {
		SpringApplication.run(DatarTwitterApplication.class, args);
	}

}
